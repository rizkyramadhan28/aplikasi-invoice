<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false,
  ]);

Route::get('/home', 'HomeController@index')->name('home');

Route::group(["middleware" => "auth"], function() {
    Route::resource('product', 'ProductController')->except(["show"]);
    Route::resource('customer', 'CustomerController')->except(["show"]);
    Route::resource('invoice', 'InvoiceController')->except(["show"]);
    Route::delete('/invoice/{id}', 'InvoiceController@destroyInvoiceDetail')->name("invoice.destroy.detail");
    Route::get('/invoice/{id}/print', 'InvoiceController@generateInvoice')->name("invoice.print");
});

Route::any('{query}', function() { return redirect('/'); })->where('query', '.*');