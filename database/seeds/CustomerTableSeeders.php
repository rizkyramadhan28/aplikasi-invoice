<?php

use App\Customer;
use Illuminate\Database\Seeder;

class CustomerTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Customer::create([
            "name" => "Rizky Ramadhan",
            "phone" => "0812345678910",
            "address" => "Purwokerto",
            "email" => "rizkyramadhan@gmail.com"
        ]);

        Customer::create([
            "name" => "Jessica Veranda",
            "phone" => "0812345678910",
            "address" => "Jakarta",
            "email" => "jessicaveranda@gmail.com"
        ]);
    }
}