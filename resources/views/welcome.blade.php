<!doctype html>
<html lang="en">

<head>
    <title>Aplikasi Invoice</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:300,400,700,800|Open+Sans:300,400,700"
        rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/aos.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/jquery.timepicker.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/fonts/ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/fonts/fontawesome/css/font-awesome.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/fonts/flaticon/font/flaticon.css') }}">

    <!-- Theme Style -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

</head>

<body class="bg-light">

    <body data-spy="scroll" data-target="#ftco-navbar-spy" data-offset="0">

        <div class="site-wrap">
            <header class="site-header">
                <div class="row align-items-center">
                    <div class="col-5 col-md-3">
                    </div>

                    <div class="col-2 col-md-6 text-center site-logo-wrap">
                        <a href="{{ url('/') }}" class="site-logo">I</a>
                    </div>
                </div>
            </header> <!-- site-header -->

            <div class="main-wrap " id="section-home">

                <div class="cover_1 overlay bg-light">
                    <div class="img_bg" style="background-image: url({{ asset('assets/images/invoice.jpg') }});"
                        data-stellar-background-ratio="0.5">
                        <div class="container">
                            <div class="row align-items-center justify-content-center text-center">
                                <div class="col-md-10" data-aos="fade-up">
                                    <h2 class="heading mb-5">Aplikasi Invoice</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .cover_1 -->

                <div class="section pb-3 bg-white" id="section-about" data-aos="fade-up">
                    <div class="container">
                        <div class="row align-items-center justify-content-center">
                            <div class="col-md-12 col-lg-8 section-heading">
                                <h2 class="heading mb-5">About</h2>

                                <p>Aplikasi ini berguna untuk mendata setiap tagihan produk yang dibeli oleh pelanggan.
                                    Terdapat beberapa menu untuk melakukan manajemen, seperti manajemen invoice,
                                    pelanggan dan produk. Selain itu, terdapat juga fitur untuk mencetak setiap invoice
                                    yang diinginkan.</p>
                            </div>
                        </div>
                    </div>
                </div> <!-- .section -->

                <div class="section bg-white" data-aos="fade-up">
                    <div class="container">
                        <div class="row mb-5">
                            <div class="col-md-12 section-heading text-center">
                                <h2 class="heading mb-5">Author</h2>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 col-lg-6 pr-lg-5 mx-auto text-center mb-5">
                                <div class="ftco-38">
                                    <div class="ftco-38-img">
                                        <div class="ftco-38-header">
                                            <img src="{{ asset('assets/images/author.jpg') }}" alt="Image">
                                            <h3 class="ftco-38-heading">Rizky Ramadhan</h3>
                                            <p class="ftco-38-subheading">Web Developer</p>
                                        </div>

                                        <div class="ftco-38-body">
                                            <p>Rizky Ramadhan adalah seorang web developer. Saat ini berkuliah di
                                                Institut Teknologi Telkom Purwokerto jurusan Teknik Informatika.
                                                Tertarik dan mendalami pemrograman sejak SMK. Rizky Ramadhan pernah
                                                dipercaya untuk mengajar mata kuliah Pemrograman Dasar
                                                sebagai asisten dosen. Selain itu, ia juga sering menggunakan sisa waktu
                                                luang untuk menulis tutorial pemrograman di blog pribadinya.
                                            </p>

                                            <a href="https://www.facebook.com/thenekolite/" class="p-2"><span
                                                    class="fa fa-facebook"></span></a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-md-4"></div> -->
                        </div>
                    </div>
                </div> <!-- .section -->

                <footer class="ftco-footer">
                    <div class="container">
                        <div class="row pt-5">
                            <div class="col-md-12 text-center">
                                <p>
                                    Copyright &copy;<script>
                                        document.write(new Date().getFullYear());
                                    </script> All rights reserved
                                </p>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>

            <!-- loader -->
            <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
                    <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
                    <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                        stroke="#ff7a5c" /></svg></div>

            <script src="{{ asset('assets/js/jquery-3.2.1.min.js') }}"></script>
            <script src="{{ asset('assets/js/jquery-migrate-3.0.1.min.js') }}"></script>
            <script src="{{ asset('assets/js/popper.min.js') }}"></script>
            <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
            <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
            <script src="{{ asset('assets/js/jquery.waypoints.min.js') }}"></script>

            <script src="{{ asset('assets/js/bootstrap-datepicker.js') }}"></script>
            <script src="{{ asset('assets/js/jquery.timepicker.min.js') }}"></script>
            <script src="{{ asset('assets/js/jquery.stellar.min.js') }}"></script>

            <script src="{{ asset('assets/js/jquery.easing.1.3.js') }}"></script>

            <script src="{{ asset('assets/js/aos.js') }}"></script>


            <script
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false">
            </script>

            <script src="{{ asset('assets/js/main.js') }}"></script>
    </body>

</html>