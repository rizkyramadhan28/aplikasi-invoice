@extends('layouts.app')

@section('title')
<title>Edit Pelanggan</title>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Edit Pelanggan</h3>
                </div>

                <div class="card-body">
                    <form action="{{ route('customer.update', $customer->id) }}" method="POST">
                        @csrf
                        @method("PUT")

                        <div class="form-group">
                            <label for="name">Nama Pelanggan</label>
                            <input type="text" name="name" class="form-control" id="name"
                                placeholder="Masukkan nama pelanggan" value="{{ $customer->name }}">

                            <p class="text-danger">{{ $errors->first('name') }}</p>
                        </div>

                        <div class="form-group">
                            <label for="email">Email Pelanggan</label>
                            <input type="email" name="email" class="form-control" id="email"
                                placeholder="Masukkan email pelanggan" value="{{ $customer->email }}">

                            <p class="text-danger">{{ $errors->first('email') }}</p>
                        </div>

                        <div class="form-group">
                            <label for="phone">No Telepon</label>
                            <input type="number" name="phone" class="form-control" id="phone"
                                placeholder="Masukkan nomor telepon pelanggan" value="{{ $customer->phone }}">

                            <p class="text-danger">{{ $errors->first('phone') }}</p>
                        </div>

                        <div class="form-group">
                            <label for="address">Alamat Pelanggan</label>
                            <textarea name="address" class="form-control" id="address" rows="3"
                                placeholder="Masukkan alamat pelanggan"
                                value="{{ $customer->address }}">{{ $customer->address }}</textarea>

                            <p class="text-danger">{{ $errors->first('address') }}</p>
                        </div>

                        <button type="submit" class="btn btn-primary btn-sm float-right">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection