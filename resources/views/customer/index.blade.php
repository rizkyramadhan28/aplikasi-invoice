@extends('layouts.app')

@section('title')
<title>Manajemen Pelanggan</title>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h3 class="card-title">Manajemen Pelanggan</h3>
                        </div>

                        <div class="col">
                            <a href="{{ route('customer.create') }}" class="btn btn-primary btn-sm float-right">Tambah
                                Pelanggan</a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    @if (session("success"))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session("success") }}

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif

                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Pelanggan</th>
                                <th>No Telepon</th>
                                <th>Alamat</th>
                                <th>Email</th>
                                <th colspan="2" class="text-center">Aksi</th>
                            </tr>
                        </thead>

                        <tbody>
                            @forelse ($customers as $customer)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $customer->name }}</td>
                                <td>{{ $customer->phone }}</td>
                                <td>{{ $customer->address }}</td>
                                <td>{{ $customer->email }}</td>
                                <td>
                                    <form action="{{ route('customer.destroy', $customer->id) }}" method="POST">
                                        @csrf
                                        @method("DELETE")

                                        <a href="{{ route('customer.edit', $customer->id) }}"
                                            class="btn btn-warning btn-sm">Edit</a>

                                        <button class="btn btn-danger btn-sm">Hapus</button>
                                    </form>
                                </td>

                                <td>
                                    <form action="{{ route('invoice.store') }}" method="POST">
                                        @csrf

                                        <input type="hidden" name="customer_id" value="{{ $customer->id }}"
                                            class="form-control">
                                        <button class="btn btn-primary btn-sm">Buat Invoice</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-center" colspan="6">Tidak ada data</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>

                    <div class="float-right">
                        {{ $customers->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection