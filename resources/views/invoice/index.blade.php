@extends('layouts.app')

@section('title')
<title>Manajemen Invoice</title>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h3 class="card-title">Manajemen Invoice</h3>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    @if (session("success"))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session("success") }}

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif

                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Lengkap</th>
                                <th>No Telepon</th>
                                <th>Total Item</th>
                                <th>Subtotal</th>
                                <th>Pajak</th>
                                <th>Total</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>

                        <tbody>
                            @forelse ($invoices as $invoice)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $invoice->customer->name }}</td>
                                <td>{{ $invoice->customer->phone }}</td>
                                <td>{{ $invoice->details->count() }}</td>
                                <td>{{ number_format($invoice->total) }}</td>
                                <td>{{ number_format($invoice->tax) }}</td>
                                <td>{{ number_format($invoice->total_price) }}</td>
                                <td>
                                    <form action="{{ route('invoice.destroy', $invoice->id) }}" method="POST">
                                        @csrf
                                        @method("DELETE")

                                        <a href="{{ route('invoice.print', $invoice->id) }}"
                                            class="btn btn-primary btn-sm">Print</a>

                                        <a href="{{ route('invoice.edit', $invoice->id) }}"
                                            class="btn btn-warning btn-sm">Edit</a>

                                        <button class="btn btn-danger btn-sm">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-center" colspan="8">Tidak ada data</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>

                    <div class="float-right">
                        {{ $invoices->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection