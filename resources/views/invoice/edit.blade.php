@extends('layouts.app')

@section('title')
<title>Edit Invoice</title>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    @if (session("success"))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session("success") }}

                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif

                    <div class="row">
                        <div class="col">
                            <table>
                                <tr>
                                    <td>Pelanggan</td>
                                    <td>:</td>
                                    <td>{{ $invoice->customer->name }}</td>
                                </tr>

                                <tr>
                                    <td>Alamat</td>
                                    <td>:</td>
                                    <td>{{ $invoice->customer->address }}</td>
                                </tr>

                                <tr>
                                    <td>No Telepon</td>
                                    <td>:</td>
                                    <td>{{ $invoice->customer->phone }}</td>
                                </tr>

                                <tr>
                                    <td>Email</td>
                                    <td>:</td>
                                    <td>{{ $invoice->customer->email }}</td>
                                </tr>
                            </table>
                        </div>

                        <div class="col">
                            <table>
                                <tr>
                                    <td>Perusahaan</td>
                                    <td>:</td>
                                    <td>thenekolites</td>
                                </tr>

                                <tr>
                                    <td>Alamat</td>
                                    <td>:</td>
                                    <td>Jl. Gerilya Timur Purwokerto Selatan</td>
                                </tr>

                                <tr>
                                    <td>No Telepon</td>
                                    <td>:</td>
                                    <td>081234567890</td>
                                </tr>

                                <tr>
                                    <td>Email</td>
                                    <td>:</td>
                                    <td>support@thenekolites.com</td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    {{-- Menambahkan produk untuk invoice --}}

                    <div class="row mt-5">
                        <div class="col">
                            <form action="{{ route('invoice.update', $invoice->id) }}" method="POST">
                                @csrf
                                @method("PUT")

                                <table class="table table-bordered">
                                    <tr>
                                        <td>
                                            <select name="product_id" class="form-control">
                                                @foreach ($products as $product)
                                                <option value="{{ $product->id }}">{{ $product->title }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </td>

                                        <td colspan="3">
                                            <input type="number" name="qty" id="qty" min="1" value="1"
                                                class="form-control" required>
                                        </td>

                                        <td>
                                            <button type="submit" class="btn btn-primary btn-sm">Tambah</button>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>

                    {{-- Daftar invoice yang ditambahkan --}}

                    <div class="row">
                        <div class="col">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <td>#</td>
                                        <td>Produk</td>
                                        <td>Qty</td>
                                        <td>Harga</td>
                                        <td>Subtotal</td>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @forelse ($invoice->details as $detail)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $detail->product->title }}</td>
                                        <td>{{ $detail->qty }}</td>
                                        <td>Rp {{ number_format($detail->price) }}</td>
                                        <td>Rp {{ $detail->subtotal }}</td>
                                        <td>
                                            <form action="{{ route('invoice.destroy.detail', $detail->id) }}"
                                                method="POST">
                                                @csrf
                                                @method("DELETE")

                                                <button class="btn btn-danger btn-sm">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td class="text-center" colspan="6">Data kosong</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-lg-4 offset-lg-8">
                            <p><b>Total Akhir</b></p>

                            <table class="table table-hover table-bordered">
                                <tr>
                                    <td>Subtotal</td>
                                    <td>:</td>
                                    <td>{{ number_format($invoice->total) }}</td>
                                </tr>

                                <tr>
                                    <td>Pajak</td>
                                    <td>:</td>
                                    <td>2% (Rp {{ number_format($invoice->tax) }})</td>
                                </tr>

                                <tr>
                                    <td>Total</td>
                                    <td>:</td>
                                    <td>Rp {{ number_format($invoice->total_price) }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection