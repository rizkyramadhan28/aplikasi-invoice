@extends('layouts.app')

@section('title')
<title>Edit Produk</title>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Edit Produk</h3>
                </div>

                <div class="card-body">
                    <form action="{{ route('product.update', $product->id) }}" method="POST">
                        @csrf
                        @method("PUT")

                        <div class="form-group">
                            <label for="title">Nama Produk</label>
                            <input type="text" name="title" class="form-control" id="title"
                                placeholder="Masukkan nama produk" value="{{ $product->title }}">

                            <p class="text-danger">{{ $errors->first('title') }}</p>
                        </div>

                        <div class="form-group">
                            <label for="description">Deskripsi Produk</label>
                            <textarea name="description" class="form-control" id="description" rows="3"
                                placeholder="Masukkan deskripsi produk"
                                value="{{ $product->description }}">{{ $product->description }}</textarea>

                            <p class="text-danger">{{ $errors->first('description') }}</p>
                        </div>

                        <div class="form-group">
                            <label for="price">Harga Produk</label>
                            <input type="number" name="price" class="form-control" id="price"
                                placeholder="Masukkan harga produk" value="{{ $product->price }}">

                            <p class="text-danger">{{ $errors->first('price') }}</p>
                        </div>

                        <div class="form-group">
                            <label for="stock">Stok Produk</label>
                            <input type="number" name="stock" class="form-control" id="stock"
                                placeholder="Masukkan stok produk" value="{{ $product->stock }}">

                            <p class="text-danger">{{ $errors->first('stock') }}</p>
                        </div>

                        <button type="submit" class="btn btn-primary btn-sm float-right">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection