<?php

namespace App\Http\Controllers;

use PDF;
use App\Invoice;
use App\Customer;
use App\Product;
use App\Invoice_detail;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = Invoice::with(["customer", "details"])->orderBy("created_at", "DESC")->paginate(10);

        return view("invoice.index", compact("invoices"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::orderBy("created_at", "DESC")->get();

        return view("invoice.create", compact("customers"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "customer_id" => "required|exists:customers,id"
        ]);

        $invoice = Invoice::create([
            "customer_id" => $request->customer_id,
            "total" => 0
        ]);

        return redirect(route("invoice.edit", $invoice->id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        // Menggunakan relationship untuk mendapatkan data customer.

        $invoice = Invoice::with(["customer", "details", "details.product"])->findOrFail($invoice->id);
        $products = Product::orderBy("created_at", "DESC")->get();

        return view("invoice.edit", compact("invoice", "products"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        $this->validate($request, [
            "product_id" => "required|exists:products,id",
            "qty" => "required|integer"
        ]);

        $product = Product::findOrFail($request->product_id);
        $invoice_detail = $invoice->details()->where("product_id", $product->id)->first();

        if ($invoice_detail) {
            $invoice_detail->update([
                "qty" => $invoice_detail->qty + $request->qty
            ]);
        } else {
            Invoice_detail::create([
                "invoice_id" => $invoice->id,
                "product_id" => $product->id,
                "price" => $product->price,
                "qty" => $request->qty
            ]);
        }

        return redirect()->back()->with(["success" => "Produk berhasil ditambahkan."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        $invoice->delete();

        return redirect(route("invoice.index"))->with(["success" => "Invoice berhasil dihapus."]);
    }

    public function destroyInvoiceDetail($id) {
        $invoice_detail = Invoice_detail::findOrFail($id);
        $invoice_detail->delete();

        return redirect()->back()->with(["success" => "Produk berhasil dihapus."]);
    }

    public function generateInvoice($id) {
        $invoice = Invoice::with(["customer", "details", "details.product"])->findOrFail($id);

        $pdf = PDF::loadView("invoice.print", compact("invoice"))->setPaper('a4', 'landscape');

        return $pdf->stream();
    }
}