<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::orderBy("created_at", "DESC")->paginate(10);

        return view("customer.index", compact("customers"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("customer.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|string",
            "phone" => "required|max:13",
            "address" => "required|string",
            "email" => "required|email|string|unique:customers,email"
        ]);

        Customer::create($request->except("_token"));

        return redirect(route("customer.index"))->with(["success" => "Pelanggan berhasil ditambahkan."]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        return view("customer.edit", compact("customer"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $this->validate($request, [
            "name" => "required|string",
            "phone" => "required|max:13",
            "address" => "required|string",
            "email" => "required|email|string|unique:customers,email," . $customer->id
        ]);

        $customer->update($request->except("_token"));

        return redirect(route("customer.index"))->with(["success" => "Pelanggan berhasil diubah."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $customer->delete();

        return redirect(route("customer.index"))->with(["success" => "Pelanggan berhasil dihapus."]);
    }
}