<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice_detail extends Model
{
    protected $fillable = ["invoice_id", "product_id", "price", "qty"];

    // Relationship.
    // Agar Invoice_detail dapat menggunakan data invoice.

    public function invoice() {
        return $this->belongsTo(Invoice::class);
    }

    // Relationship.
    // Agar Invoice_detail dapat menggunakan data product.

    public function product() {
        return $this->belongsTo(Product::class);
    }

    // Accessor.
    // Akan menghasilkan sebuah filed bernama subtotal.
    // Dapat diakses dengan cara invoiceDetail->subtotal.

    public function getSubtotalAttribute() {
        return number_format($this->qty * $this->price);
    }
}