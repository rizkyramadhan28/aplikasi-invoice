<?php

namespace App\Providers;

use App\Invoice_detail;
use Illuminate\Support\ServiceProvider;
use App\Observers\Invoice_detailObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Panggil Invoice_detailObserver agar bekerja.

        Invoice_detail::observe(Invoice_detailObserver::class);
    }
}