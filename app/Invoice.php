<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = ["customer_id", "total"];

    // Agar invoice atau nota dapat mengakses data customer.

    public function customer() {
        return $this->belongsTo(Customer::class);
    }

    // Agar invoice dapat mengakses data invoice detail

    public function details() {
        return $this->hasMany(Invoice_detail::class);
    }

    // Accessor
    // Akan menghasilkan sebuah filed bernama tax.
    // Diakses dengan cara $invoice->tax.

    public function getTaxAttribute() {
        return ($this->total * 2) / 100;
    }

    // Accessor.
    // Akan menghasilkan sebuah field bernama total_price.
    // Diakses dengan cara $invoice->total_price.

    public function getTotalPriceAttribute() {
        return ($this->total + (($this->total * 2) / 100));
    }
}