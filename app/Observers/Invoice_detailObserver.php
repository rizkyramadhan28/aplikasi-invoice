<?php

namespace App\Observers;

use App\Invoice;
use App\Invoice_detail;

class Invoice_detailObserver
{
    // Observer berguna untuk mengubah data ketika trigger terjadi.
    // Trigger berupa penambahan, update atau hapus data.

    private function generateTotal($invoiceDetail) {
        $invoice_id = $invoiceDetail->invoice_id;

        // Mengambil semua invoice detail yang memiliki id sama dengan invoice_id

        $invoice_detail = Invoice_detail::where("invoice_id", $invoice_id)->get();
        
        // Menjumlahkan semua harga barang

        $total = $invoice_detail->sum(function($detail) {
            return $detail->qty * $detail->price;
        });

        // Mengupdate total barang

        $invoiceDetail->invoice()->update([
            "total" => $total
        ]);
    }

    /**
     * Handle the invoice_detail "created" event.
     *
     * @param  \App\Invoice_detail  $invoiceDetail
     * @return void
     */
    public function created(Invoice_detail $invoiceDetail)
    {
        $this->generateTotal($invoiceDetail);
    }

    /**
     * Handle the invoice_detail "updated" event.
     *
     * @param  \App\Invoice_detail  $invoiceDetail
     * @return void
     */
    public function updated(Invoice_detail $invoiceDetail)
    {
        $this->generateTotal($invoiceDetail);
    }

    /**
     * Handle the invoice_detail "deleted" event.
     *
     * @param  \App\Invoice_detail  $invoiceDetail
     * @return void
     */
    public function deleted(Invoice_detail $invoiceDetail)
    {
        $this->generateTotal($invoiceDetail);
    }

    /**
     * Handle the invoice_detail "restored" event.
     *
     * @param  \App\Invoice_detail  $invoiceDetail
     * @return void
     */
    public function restored(Invoice_detail $invoiceDetail)
    {
        //
    }

    /**
     * Handle the invoice_detail "force deleted" event.
     *
     * @param  \App\Invoice_detail  $invoiceDetail
     * @return void
     */
    public function forceDeleted(Invoice_detail $invoiceDetail)
    {
        //
    }
}